class Game:
    def __init__(self):
        self.pin_counts = []

    def roll(self, pins: int):
        self.pin_counts.append(pins)

    def score(self) -> int:
        roll_1, roll_2 = self.pin_counts[0], self.pin_counts[1]
        score = roll_1 + roll_2
        last_is_strike = False
        last_is_spare = False
        for pins in self.pin_counts[2:]:
            if roll_1 == 10:                # Strike
                score += roll_2 + pins
                last_is_strike = True
            elif roll_1 + roll_2 == 10:     # spare
                score += pins
                last_is_spare = True
            else:
                last_is_strike = False
                last_is_spare = False
            score += pins
            roll_1, roll_2 = roll_2, pins

        if last_is_strike:
            score -= self.pin_counts[-1]
            score -= self.pin_counts[-2]
        elif last_is_spare:
            score -= self.pin_counts[-1]

        return score
