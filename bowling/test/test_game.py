from game import Game


def roll_many(game, pin_count, roll_count=20):
    for roll in range(roll_count):
        game.roll(pin_count)


def test_null_game():
    game = Game()
    roll_many(game, 0)
    score = game.score()
    assert score == 0


def test_1_game():
    game = Game()
    roll_many(game, 1)
    score = game.score()
    assert score == 20


def test_spare():
    game = Game()
    game.roll(5)
    game.roll(5)  # spare
    game.roll(3)
    roll_many(game, 0, roll_count=17)
    score = game.score()
    assert score == 16


def test_strike():
    game = Game()
    game.roll(10)  # strike
    game.roll(3)
    game.roll(4)
    roll_many(game, 0, roll_count=16)
    score = game.score()
    assert score == 24


def test_top_game():
    game = Game()
    roll_many(game, 10, roll_count=12)
    score = game.score()
    assert score == 300