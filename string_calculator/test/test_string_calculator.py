import pytest

from string_calculator import string_calculator

"""
An empty string returns zero
A single number returns the value
Two numbers, comma delimited, returns the sum
Two numbers, newline delimited, returns the sum
Three numbers, delimited either way, returns the sum
Negative numbers throw an exception
Numbers greater than 1000 are ignored
A single char delimiter can be defined on the first line (e.g. //# for a ‘#’ as the delimiter)
A multi char delimiter can be defined on the first line (e.g. //[###] for ‘###’ as the delimiter)
Many single or multi-char delimiters can be defined (each wrapped in square brackets)"""


def test_empty_string():
    calc = string_calculator.StringCalculator('')
    assert calc.calc() == 0


def test_23():
    calc = string_calculator.StringCalculator('1,2')
    assert calc.calc() == 3
    calc = string_calculator.StringCalculator('1\n2')
    assert calc.calc() == 3
    calc = string_calculator.StringCalculator('1,2\n3')
    assert calc.calc() == 6


def test_neg():
    calc = string_calculator.StringCalculator('1,-2')
    with pytest.raises(Exception):
        calc.calc()


def test_ignore_big():
    calc = string_calculator.StringCalculator('1,1001')
    assert calc.calc() == 1


def test_first_line_delimiter():
    calc = string_calculator.StringCalculator('//#\n1#2')
    assert calc.calc() == 3
