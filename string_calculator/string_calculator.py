
class StringCalculator:
    def __init__(self, strin):
        self.delimiter = None
        if strin.startswith('//'):
            lines = strin.split('\n')
            self.delimiter = lines[0][2:]
            strin = ''.join(lines[1:])
        self.split_vals = strin

    @property
    def split_vals(self):
        return self._split_vals

    @split_vals.setter
    def split_vals(self, strin):
        if self.delimiter:
            strin = strin.replace(self.delimiter, ' ')
        self._split_vals = strin.replace(',', ' ').replace('\n', ' ').split()

    def calc(self):
        total = 0
        for e in self.split_vals:
            ev = int(e)
            if ev > 1000:
                continue
            elif ev >= 0:
                total += ev
            else:
                raise Exception("Neg value")
        return total
