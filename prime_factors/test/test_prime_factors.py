from prime_factors import prime_factors


def test_empty():
    fs = prime_factors.PrimeFactors.generate(1)
    assert fs == []


def test_first_primes():
    for p in [2, 3, 5]:
        fs = prime_factors.PrimeFactors.generate(p)
        assert fs == [p]


def test_4():
    fs = prime_factors.PrimeFactors.generate(4)
    assert fs == [2, 2]


def test_6():
    fs = prime_factors.PrimeFactors.generate(6)
    assert fs == [2, 3]


def test_9():
    fs = prime_factors.PrimeFactors.generate(9)
    assert fs == [3, 3]
