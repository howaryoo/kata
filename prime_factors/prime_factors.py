class PrimeFactors:
    @classmethod
    def generate(cls, n, prime_factors=None):
        if prime_factors is None:
            prime_factors = []
        for i in range(2, n+1):
            if n % i == 0:
                prime_factors.append(i)
                return cls.generate(n//i, prime_factors)
        return prime_factors
